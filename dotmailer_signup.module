<?php

/**
 * @file
 * DotMailer signup form creation.
 */

/**
 * Implements hook_menu().
 */
function dotmailer_signup_menu() {
  $items = array();

  if (module_exists('block')) {
    $items['admin/structure/block/add-dotmailer-signup-block'] = [
      'title' => t('Add DotMailer Signup block'),
      'description' => 'Add a new DotMailer Signup block.',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['dotmailer_signup_add_block_form'],
      'access arguments' => ['administer blocks'],
      'type' => MENU_LOCAL_ACTION,
    ];

    $default_theme = variable_get('theme_default', 'bartik');
    foreach (list_themes() as $key => $theme) {
      if ($key != $default_theme) {
        $items['admin/structure/block/list/' . $key . '/add-dotmailer-signup-block'] = [
          'title' => t('Add DotMailer Signup block'),
          'description' => 'Add a new DotMailer Signup block.',
          'page callback' => 'drupal_get_form',
          'page arguments' => ['dotmailer_signup_add_block_form'],
          'access arguments' => ['administer blocks'],
          'type' => MENU_LOCAL_ACTION,
        ];
      }
    }

    $items['admin/structure/block/delete-dotmailer-signup-block'] = [
      'title' => t('Delete DotMailer Signup block'),
      'page callback' => 'drupal_get_form',
      'page arguments' => ['dotmailer_signup_delete_form'],
      'access arguments' => ['administer blocks'],
      'type' => MENU_CALLBACK,
    ];
  }

  return $items;
}

/**
 * Menu callback: confirm deletion of menu blocks.
 */
function dotmailer_signup_delete_form($form, &$form_state, $delta = 0) {
  $title = '';
  $form['block_title'] = array(
    '#type' => 'hidden',
    '#value' => $title
  );
  $form['delta'] = array(
    '#type' => 'hidden',
    '#value' => $delta
  );

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/structure/block', NULL, t('Delete'), t('Cancel'));
}

/**
 * Implements hook_form-id_alter().
 */
function dotmailer_signup_form_block_admin_display_form_alter(&$form, $form_state) {
  $deltas = variable_get('dotmailer_signup_block_ids', array());

  if (count($deltas) > 0) {
    foreach ($deltas as $delta) {
      if (!isset($exported[$delta])) {
        $form['blocks']['dotmailer_signup_' . $delta]['delete'] = array(
          '#type' => 'link',
          '#title' => t('delete'),
          '#href' => 'admin/structure/block/delete-dotmailer-signup-block/' . $delta
        );
      }
    }
  }
}

/**
 * Menu callback: display the DotMailer Signup block addition form.
 */
function dotmailer_signup_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  $form = block_admin_configure($form, $form_state, 'dotmailer_signup', NULL);

  // Other modules should be able to use hook_form_block_add_block_form_alter()
  // to modify this form, so add a base form ID.
  $form_state['build_info']['base_form_id'] = 'block_add_block_form';

  // Prevent block_add_block_form_validate/submit() from being automatically
  // added because of the base form ID by providing these handlers manually.
  $form['#validate'] = array();
  $form['#submit'] = array('dotmailer_signup_add_block_form_submit');

  return $form;
}

/**
 * Save the new dotmailer block.
 */
function dotmailer_signup_add_block_form_submit($form, &$form_state) {
  // Determine the delta of the new block.
  $block_ids = variable_get('dotmailer_signup_block_ids', array());
  $delta = empty($block_ids) ? 1 : max($block_ids) + 1;
  $form_state['values']['delta'] = $delta;

  // Save the new array of blocks IDs.
  $block_ids[] = $delta;
  variable_set('dotmailer_signup_block_ids', $block_ids);

  // Save the block configuration.
  dotmailer_signup_block_save($delta, $form_state['values']);

  // Run the normal new block submission (borrowed from block_add_block_form_submit).
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'region', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $region = !empty($form_state['values']['regions'][$theme->name]) ? $form_state['values']['regions'][$theme->name] : BLOCK_REGION_NONE;
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'status' => (int) ($region != BLOCK_REGION_NONE),
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  drupal_set_message(t('The dotmailer block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Implements hook_block_info().
 */
function dotmailer_signup_block_info() {
  $blocks = array();

  $deltas = variable_get('dotmailer_signup_block_ids', array());

  if (count($deltas) > 0) {
    foreach ($deltas as $delta) {
      $title = variable_get('dotmailer_signup_admin_title_' . $delta, '');
      $blocks[$delta] = array(
        'info' => t('dotMailer Signup: @title', array('@title' => $title)),
        'cache' => DRUPAL_CACHE_GLOBAL,
      );
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function dotmailer_signup_block_view($delta) {
  $block = array();
  $block['subject'] = t('Newsletter');

  $form = drupal_get_form('dotmailer_signup_form', $delta);
  $block['content'] = drupal_render($form);

  return $block;
}

/**
 * Implements hook_block_configure().
 */
function dotmailer_signup_block_configure($delta = '') {
  $deltas = variable_get('dotmailer_signup_block_ids', array());

  if (is_null($delta)) {
    $delta = empty($deltas) ? 1 : max($deltas) + 1;
  }

  $dotmailer_signup_account = variable_get('dotmailer_signup_account_' . $delta, '');
  $dotmailer_signup_address_book = variable_get('dotmailer_signup_address_book_' . $delta, []);
  $dotmailer_signup_optin_type = variable_get('dotmailer_signup_optin_type_' . $delta, 'Single');
  $dotmailer_signup_audience_type = variable_get('dotmailer_signup_audience_type_' . $delta, 'Unknown');
  $dotmailer_signup_name_include = variable_get('dotmailer_signup_name_include_' . $delta, 0);
  $dotmailer_signup_name_field = variable_get('dotmailer_signup_name_field_' . $delta, '');
  $dotmailer_signup_description = variable_get('dotmailer_signup_description_' . $delta, t('Keep me posted with the latest news, promotions, products, case studies and events.'));
  $dotmailer_signup_admin_title = variable_get('dotmailer_signup_admin_title_' . $delta, '');

  $form = array();

  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => $dotmailer_signup_admin_title,
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this block. If blank, the regular title will be used.'),
  );


  $dotmailer_accounts_list = dotmailer_integration_get_account_list();

  $form['dotmailer_signup_account'] = array(
    '#type' => 'select',
    '#title' => t('Select the DotMailer account to be used'),
    '#desctiption' => t('This will default to the '),
    '#options' => $dotmailer_accounts_list,
    '#default_value' => $dotmailer_signup_account,
    '#ajax' => array(
      'callback' => 'dotmailer_signup_ajax_addressbook_callback',
      'wrapper' => 'dotmailer-signup-addressbooks-replace',
    ),
  );

  if ($dotmailer_signup_account == '' && count($dotmailer_accounts_list) > 0) {
    $dotmailer_signup_account = array_keys($dotmailer_accounts_list);
    $dotmailer_signup_account = $dotmailer_signup_account[0];
  }

  $selected_account = (isset($_POST['dotmailer_signup_account']) ? $_POST['dotmailer_signup_account'] : $dotmailer_signup_account);

  $form['dotmailer_signup_address_book'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Pick one or more address book:'),
    '#options' => _dotmailer_signup_ajax_addressbook_list($selected_account),
    '#required' => TRUE,
    '#default_value' => $dotmailer_signup_address_book,
    '#prefix' => '<div id="dotmailer-signup-addressbooks-replace">',
    '#suffix' => '</div>',
  );

  $form['dotmailer_signup_audience_type'] = array(
    '#type' => 'select',
    '#title' => t('Select audience type:'),
    '#options' => array(
      'Unknown' => 'Unknown',
      'B2C' => 'Business to Consumer',
      'B2B' => 'Business to Business',
      'B2M' => 'Business to Manager',
    ),
    '#required' => TRUE,
    '#default_value' => $dotmailer_signup_audience_type
  );

  $form['dotmailer_signup_optin_type'] = array(
    '#type' => 'select',
    '#title' => t('Select opt in type:'),
    '#options' => array(
      'Single' => 'Single',
      'Double' => 'Double'
    ),
    '#required' => TRUE,
    '#default_value' => $dotmailer_signup_optin_type,
    '#description' => 'When setting up double opt in dotMailer you can also
          set the opt in redirect page to be "subscription-confirmed"
          which is defined in this module.'
  );

  $form['dotmailer_signup_description'] = array(
    '#type' => 'textfield',
    '#title' => 'Description',
    '#default_value' => $dotmailer_signup_description,
    '#description' => 'Leave blank to disable the description text.'
  );


  $form['dotmailer_signup_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Name field'),
  );

  $form['dotmailer_signup_name']['dotmailer_signup_name_include'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include name field?'),
    '#default_value' => $dotmailer_signup_name_include
  );

  $form['dotmailer_signup_name']['dotmailer_signup_name_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Name field dotMailer id'),
    '#default_value' => $dotmailer_signup_name_field
  );

  return $form;
}

/**
 * Implements hook_block_save().
 */
function dotmailer_signup_block_save($delta = '', $edit = []) {
  variable_set('dotmailer_signup_account_' . $delta, $edit['dotmailer_signup_account']);
  variable_set('dotmailer_signup_address_book_' . $delta, $edit['dotmailer_signup_address_book']);
  variable_set('dotmailer_signup_optin_type_' . $delta, $edit['dotmailer_signup_optin_type']);
  variable_set('dotmailer_signup_audience_type_' . $delta, $edit['dotmailer_signup_audience_type']);
  variable_set('dotmailer_signup_name_include_' . $delta, $edit['dotmailer_signup_name_include']);
  variable_set('dotmailer_signup_name_field_' . $delta, $edit['dotmailer_signup_name_field']);
  variable_set('dotmailer_signup_description_' . $delta, $edit['dotmailer_signup_description']);
  variable_set('dotmailer_signup_admin_title_' . $delta, $edit['admin_title']);

  $dotmailer = dotmailer_integration_get_object();
  $address_book_objects = $dotmailer->ListAddressBooks();

  $address_books = [];

  if ($address_book_objects !== FALSE) {
    foreach ($address_book_objects as $address_book) {
      if (!empty($edit['dotmailer_signup_address_book'][$address_book->ID])) {
        $address_books[$address_book->ID] = $address_book->Name;
      }
    }
  }
  variable_set('dotmailer_signup_selected_addressbook_user_select_' . $delta, $address_books);
}

/**
 * Create a signup form for dotMailer signups.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The current form state.
 *
 * @return array
 *   The form.
 */
function dotmailer_signup_form(array $form, array &$form_state, $delta) {
  $form['error_container'] = [
    '#prefix' => '<div id="dotmailer_signup_form_error_container">',
    '#markup' => '<!-- Comment -->',
    '#suffix' => '</div>',
  ];

  $form['#prefix'] = '<div id="dotmailer_signup_form_wrapper" >';
  $form['#suffix'] = '</div>';

  if (variable_get('dotmailer_signup_name_include_' . $delta, 0) == 1) {
    // Only show the name field if previously selected.
    $form['dotmailer_signup_name'] = [
      '#type' => 'textfield',
      '#title' => t('Your Name'),
      '#required' => TRUE,
      '#size' => NULL,
      '#attributes' => [
        'placeholder' => 'Your Name',
        'required' => 'required',
      ],
    ];
  }

  $form['dotmailer_signup_email'] = [
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#required' => TRUE,
    '#size' => NULL,
    '#attributes' => [
      'placeholder' => 'Email Address',
      'required' => 'required',
    ],
  ];

  $adressbooks = variable_get('dotmailer_signup_selected_addressbook_user_select_' . $delta, []);

  if (count($adressbooks) !== 1) {
    $form['dotmailer_signup_addressbook'] = [
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#title' => t('Select a newsletter'),
      '#options' => $adressbooks,
    ];
  }

  $form['dotmailer_signup_submit'] = [
    '#type' => 'submit',
    '#value' => t('Subscribe'),
    '#attributes' => [
      'class' => ['btn', 'btn-primary'],
    ],
    '#prefix' => '<div id="form-actions" class="form-actions">',
    '#markup' => '',
    '#suffix' => '</div>',
    '#ajax' => [
      'callback' => 'dotmailer_signup_form_ajax_handler',
      'effect' => 'fade',
    ],
  ];

  // Check if Honeypot module exists and add form protection.
  if (module_exists('honeypot')) {
    honeypot_add_form_protection($form, $form_state, ['honeypot', 'time_restriction']);
  }
  return $form;
}

/**
 * Implements hook_FORM_ID_validate().
 */
function dotmailer_signup_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['dotmailer_signup_email']) && !filter_var($form_state['values']['dotmailer_signup_email'], FILTER_VALIDATE_EMAIL)) {
    // Ensure e-mail is valid.
    form_set_error('dotmailer_signup_email', t("The email address you entered is not valid. Please check and try again."));
    return FALSE;
  }
}

/**
 * Implements hook_FORM_ID_submit().
 *
 * @throws \Drupal\dotmailer_integration\DotMailer\Exception\MissingRequiredParametersException
 */
function dotmailer_signup_form_submit($form, &$form_state) {
  // Grab the delta.
  $delta = $form_state['values']['dotmailer_signup_delta'];

  // Grab the rest of the information for this block.
  $dotmailer_signup_account = variable_get('dotmailer_signup_account_' . $delta, '');
  $dotmailer_signup_address_book = variable_get('dotmailer_signup_address_book_' . $delta, '');
  $dotmailer_signup_optin_type = variable_get('dotmailer_signup_optin_type_' . $delta, 'Single');
  $dotmailer_signup_audience_type = variable_get('dotmailer_signup_audience_type_' . $delta, 'Unknown');
  $dotmailer_signup_name_include = variable_get('dotmailer_signup_name_include_' . $delta, 0);
  $dotmailer_signup_name_field = variable_get('dotmailer_signup_name_field_' . $delta, '');

  $form_state['dotmailer_signup_error'] = FALSE;

  // Connect to Dotmailer Integration API.
  $dotmailer_api = dotmailer_integration_get_object($dotmailer_signup_account);

  if ($dotmailer_api === FALSE) {
    watchdog('dotMailer signup subscription error: no dotMailer object found', 'error');
    drupal_set_message(t('An error occurred whilst processing your signup request, please try again later.'), 'error');
    $form_state['dotmailer_signup_error'] = TRUE;
    return;
  }

  if ($dotmailer_signup_address_book == 0) {
    watchdog('dotmailer_signup', 'dotMailer signup subscription error: invalid address book ID', array(), WATCHDOG_ALERT);
    drupal_set_message(t('An error occurred whilst processing your signup request, please try again later.'), 'error');
    $form_state['dotmailer_signup_error'] = TRUE;
    return;
  }

  $fields = [];

  if (isset($form_state['values']['dotmailer_signup_name']) && trim($form_state['values']['dotmailer_signup_name']) != '' && $dotmailer_signup_name_include == 1) {
    $name = $form_state['values']['dotmailer_signup_name'];
    $name_field = $dotmailer_signup_name_field;
    if ($name_field != '') {
      $fields = array(
        $name_field => $name,
      );
    }
  }
  else {
    $name = FALSE;
  }

  $adressbooks = variable_get('dotmailer_signup_selected_addressbook_user_select', []);
  $email = $form_state['values']['dotmailer_signup_email'];

  $contact = array(
    'Email' => $email,
    'AudienceType' => $dotmailer_signup_audience_type,
    'OptInType' => $dotmailer_signup_optin_type,
    'EmailType' => 'Html',
  );

  // Check if user exists within dotMailer.
  $dotmailer_user_exists = $dotmailer_api->GetContactByEmail($email);

  $dotmailer_user_to_address_book = FALSE;

  // If user exists, check if they are aleady in the address book.
  if ($dotmailer_user_exists) {
    // Get address books that the contact is subscribed to.
    $dotmailer_contacts_address_books = $dotmailer_api->ListAddressBooksForContact($dotmailer_user_exists);

    if ($dotmailer_contacts_address_books) {
      if (count($adressbooks) !== 1) {
        foreach ($dotmailer_contacts_address_books as $address_book) {
          foreach ($form_state['values']['addressbook'] as $address_book_id) {
            if ($address_book_id > 0 && $address_book->ID != $address_book_id) {
              $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($contact, $fields, $address_book_id);
            }
          }
        }
      }
      else {
        foreach ($adressbooks as $address_book_key => $adress_book_value) {
          $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($contact, $fields, $address_book_key);
        }
      }
    }
  }
  else {
    if (count($adressbooks) !== 1) {
      foreach ($form_state['values']['addressbook'] as $address_book_id) {
        if ($address_book_id > 0) {
          $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($contact, $fields, $address_book_id);
        }
      }
    }
    else {
      foreach ($adressbooks as $address_book_key => $adress_book_value) {
        $dotmailer_user_to_address_book = $dotmailer_api->AddContactToAddressBook($contact, $fields, $address_book_key);
      }
    }
  }

  // Ensure user has been subscribed to address book.
  if (!$dotmailer_user_to_address_book) {
    watchdog('dotmailer_signup', 'Could not subscribe @name with email: @email', ['@name' => $name, '@email' => $email], WATCHDOG_ERROR);
    $form_state['dotmailer_signup_error'] = TRUE;
  }
}

/**
 * Implements hook_FORM_ID_submit().
 *
 * @throws Exception
 */
function dotmailer_signup_form_ajax_handler($form, &$form_state) {
  $commands = [];

  if (!empty(form_get_errors())) {
    $messages = theme('status_messages');
    $commands[] = ajax_command_html('#dotmailer_signup_form_error_container', $messages);
  }
  else {
    $page_body = variable_get('dotmailer_signup_confirmation_text_field', ['value' => '', 'format' => 'check_plain']);
    $commands[] = ajax_command_replace('#dotmailer_signup_form_wrapper',
      '<div id="messages">' . check_markup($page_body['value'], $page_body['format']) . '</div>');
  }

  // 3) Return our ajax commands.
  return ['#type' => 'ajax', '#commands' => $commands];
}


/**
 * @param $account_name
 * @return array
 */
function _dotmailer_signup_ajax_addressbook_list($account_name) {

  if ($account_name == '') {
    return array();
  }

  $dotmailer = dotmailer_integration_get_object($account_name);

  if ($dotmailer == FALSE) {
    return array();
  }

  $address_book_objects = $dotmailer->ListAddressBooks();

  $address_books = array();

  if ($address_book_objects !== FALSE && count($address_book_objects) > 0) {
    foreach ($address_book_objects as $address_book) {
      $address_books[$address_book->ID] = $address_book->Name;
    }
  }

  return $address_books;
}
